#!/bin/bash

get_available_images(){
  if [[ -z "$RELEASE_PATH" ]]; then
    logE "Env not initialized !"
    return
  fi
  find "$RELEASE_PATH" -mindepth 1 -maxdepth 1 -type d -printf '%f\n'
}

image-set-layers(){
  logD "Deploying custom layers"
  if [[ -z "$_added_layers" ]]; then
    logW "No layers to add..."
    return
  fi

  for l in $_added_layers
  do
    logD "-- Layer $l"
    if ! [[ -e "$l" ]]; then
      logD  "The layer $l does not exist... Skiping"
      continue
    fi

    logD "Unpacking $l"
    tar -xvf "$l" -C $_image_path

    if [[ "$?" != "0" ]]; then
      logE "Impossible to extract $l. Is the file path correct ?"
      return
    fi
    logD "Extracting $l FINISHED !"
  done

}

image-set-network(){

  logD "Deploying network layer ..."

  if [[ "$_net_type" == "private" ]]; then
    logD "Private network nothing to do..."
    return
  fi

  if [[ "$_net_type" == "shared" ]]; then
    logD "Shared network. Let's prepare host."
    _net_ip=$(generate_ip)
    logD "Generated Ip : $_net_ip"
    mount_lo_interface $_net_ip
  fi

  logD "Exposing needed ports :) "
  expose_port

  if [[ -n $_name_line ]]; then
    logD "The service name is $_name_line ip can be used to resolve $_net_ip"
    echo "$_net_ip $_name_line" >> /etc/hosts
  fi

}

load_image_configuration(){
  unload_image_configuration
  if [[ -z "$1" ]]; then
    logE "You must give a configuration to load..."
    return
  fi
  source "$1"

  VOLUME "/etc/hosts:/etc/hosts"

}

unload_image_configuration(){
  _env_line=()
  _volume_line=""
  _volumero_line=""
  _name_line=""
  _entry_point_line=""
  _net_line=""
  _service_name=""
  _net_ip="127.0.0.1"
  _net_type="private"
  _proxy_backend="apache"
  _source_image=""

  _exposed_ports=""
  _config_set=()
  _added_layers=""
}

start_image(){
  local imagePath=$1

  local bindLine=""
  local netLine=""
  local nameLine=""
  local envLine=""
  local volumeLine=""

  if [[ -z "$imagePath" ]]; then
    logE "Pleaze specify an image"
    return
  fi

  logD "Volumes : $_volume_line"
  volumeLine=""
  if [[ -n $_volume_line ]]; then
    volumeLine=$(echo $_volume_line | tr ' ' '\n' | sed -r 's/(.*?)/--bind=\1 /' | tr '\n' ' ')
  fi

  logD "Volumes RO : $_volumero_line"
  volumeroLine=""
  if [[ -n $_volumero_line ]]; then
    volumeroLine=$(echo $_volumero_line | tr ' ' '\n' | sed -r 's/(.*?)/--bind-ro=\1 /' | tr '\n' ' ')
  fi

  envLine=""
  logD "The net ip is $_net_ip"
  _env_line+=("IP=$_net_ip")

  if [[  ${#_env_line[@]} -gt "0" ]]; then
    local env_n=${#_env_line[@]}
    for (( cu=0 ; cu <$env_n ; cu++ ))
    do
      envLine=" $envLine --setenv=${_env_line[$cu]} "
    done
  fi

  netLine=$_net_line
  nameLine="--machine $_name_line"

  if [[ -n $_entry_point_line ]]; then
    logI "Creating start file with the corresponding entrypoint"

    echo "#!/bin/sh" >  $imagePath/start.sh
    echo "$_entry_point_line" >>  $imagePath/start.sh
    chmod +x  $imagePath/start.sh
  fi

  logD "Here is the constructed line ! "
  echo "systemd-nspawn --capability=all $nameLine $volumeLine $volumeroLine $netLine $envLine -D $imagePath  getty -nl /start.sh  0 /dev/console"
  systemd-nspawn --capability=all $nameLine $volumeLine $volumeroLine $netLine $envLine -D $imagePath  getty -nl /start.sh  0 /dev/console
  if [[ $? -ne 0 ]]; then
    logE "Image unexpectedly finished ! "
    return
  fi
}

clear_fs(){
  local rmPath=$1
  if [[ -z "$rmPath" ]]; then
    return
  fi
  rm -rf $rmPath
}

image-set-config(){

  imagePath=$1

  if [[ -z $imagePath ]]; then
    logE "Config can't be setup ! Missing image path ! "
    return
  fi

  logD "Commands number : ${#_config_set[@]} "

  local n=$((${#_config_set[@]}+1))

  for (( it=1; it<$n ; it++ )); do
    echo  ${_config_set[$it-1]}
    set ${_config_set[$it-1]}

    if [[ $1 == "setip" ]]; then

      logD "Tring to change ip ... for $imagePath/$2"
      if [[ -f $imagePath/${2} ]]; then
        logI "Changing ip in $imagePath/${2}"
        sed -i "s/{{container_Ip}}/$_net_ip/" $imagePath/${2}
      else
        logW "File not found skipping"
      fi

    elif [[ $1 == "custom" ]]; then
      logW "This is a custom not yet implemented command... ($@)"
    fi

  done
}

image-copy(){
  local n=$((${#_copy[@]}+1))

  for (( it=1; it<$n ; it++ )); do
    echo  ${_copy[$it-1]}
    set ${_copy[$it-1]}

    if ! [[ -e "$1" ]]; then
      logW "File source $1 does not exist... Skiping Copy ... "
      continue
    fi

    logD "Copying from $1 to $_image_path/${2}"

    mkdir -p $(dirname $_image_path/$2)

    if [[ -f "$1" ]]; then
      cp -R  "$1" "$_image_path/$2"
    else
      cp -R "$1/." "$_image_path/$2"
    fi

    image-create-layer $_image_path

  done
}

image-run-commands(){
  local n=$((${#_run_cmds[@]}+1))

  local tmp_entry=$_entry_point_line
  _entry_point_line=""

  for (( it=1; it<$n ; it++ )); do
    echo "#!/bin/sh" >  $_image_path/start.sh
    echo "$_run_cmds" >> $_image_path/start.sh
    chmod +x $_image_path/start.sh

    start_image $_image_path

    rm $_image_path/start.sh

    image-create-layer $_image_path

  done

  _entry_point_line=$tmp_entry
}

image-set-entrypoint(){
  echo "#!/bin/sh" >  $_image_path/start.sh
  echo "$_entry_point_line" >> $_image_path/start.sh
  chmod +x $_image_path/start.sh

  image-create-layer $_image_path
}

image-create-layer(){
  logI "Creating layer..."
  mkdir -p $RELEASE_PATH/${_current_image}
  local number=$(find $RELEASE_PATH/${_current_image} -name "layer*" | wc -l)
  echo "tar --listed-incremental=$RELEASE_PATH/${_current_image}/snapshot.file -cf $RELEASE_PATH/${_current_image}/layer_$number.tar -C $BUILD_PATH/${_current_image} ."
  tar --listed-incremental=$RELEASE_PATH/${_current_image}/snapshot.file -cf $RELEASE_PATH/${_current_image}/layer_$number.tar -C $BUILD_PATH/${_current_image} .
  logI "Layer $number : $(du -h "$RELEASE_PATH/${_current_image}/layer_$number.tar" | cut -f1)o"
}

extract-image-layers(){
  local name=$1
  if [[ -z "$name" ]]; then
    logE "Nothing to do/extract"
    return
  fi

  local file_list=$(find "$RELEASE_PATH/$name" -name "*.tar" | sort )

  for file in $file_list
  do

    logI "Extracting $file in $_image_tmp_path"

    tar xf "$file" -C "$_image_tmp_path"

    logI "Done !"

  done

}

remove-image(){
  selectedImage=$1
  if [[ -z "$selectedImage" ]]; then
    logE "Please specify an image name"
    return
  fi

  if [[ ! -e "$RELEASE_PATH/$selectedImage" ]]; then
    logE "Image does not exit yet."
    return
  fi

  rm -rf "$RELEASE_PATH/$selectedImage"
}
