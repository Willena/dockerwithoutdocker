#!/bin/bash

# This file contain element that usefull for network management for containers
#

#IP_ATTRIBUTED_RANGE="10.9.0.0"
#MASK_ATTRIBUTED_RANGE="255.255.0.0"

get_used_address(){
  ifconfig | perl -nle 's/dr:(\S+)/print $1/e' | grep "10.9"
}

generate_ip(){
  local digit1=$(( ( RANDOM % 254 )  + 1 ))
  local digit2=$(( ( RANDOM % 254 )  + 1 ))
  local usedips=$(get_used_address)
  while get_used_address | grep "10.9.$digit1.$digit2" > /dev/null
  do
    digit1=$(( ( RANDOM % 254 )  + 1 ))
    digit2=$(( ( RANDOM % 254 )  + 1 ))
  done

  echo "10.9.$digit1.$digit2"
}

mount_lo_interface(){
  logD "Creating lo interface with ip $1"
  if [[ -z $1 ]]; then
    logW "ip not provided -- Skip !"
  fi

  local ip=$1
  local totalNumber=$(( $(get_used_address | wc -l) + 1 ))
  logD "#Iface : $totalNumber - ip: $ip"
  logD "ifconfig lo:$totalNumber $ip netmask 255.255.0.0 up"
  ifconfig lo:$totalNumber $ip netmask 255.255.0.0 up

}

umount_lo_interface(){

  local ip=$1
  if [[ -z $ip ]]; then
    echo "You must provide an ip"
  fi

  local iface=$(ifconfig | grep -B1 "$ip" | awk '$1!="inet" && $1!="--" {print $1}')

  ifconfig $iface down

}
