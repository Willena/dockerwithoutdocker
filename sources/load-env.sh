#!/bin/bash

source "utils.sh"

logI "============================"
logI "======= LOADING ENV ========"
logI "============================"
echo ""
echo ""



source "download-frozen-image-v2.sh"
source "image-management.sh"
source "build-image.sh"
source "prepare-image.sh"
source "start-container.sh"
source "configuration-management.sh"
source "interface-management.sh"
source "network-management.sh"

logI "Here is the list of image available"
get_available_images | sed 's/^/ - /'

logI "To build use build-image <conf>"
logI "To start container start-container <name> <run_conf>"

export DEFAULT_IFACE="venet0"
