#!/bin/bash

_FILE_PATH=$(dirname $0)
_COMPOSE_PATH="$_FILE_PATH/../Compose"
_SERVICE_PATH="/etc/systemd/system"
#SELECT SERVICE TO ENABLE OR NOT...

services="$(find  $_COMPOSE_PATH -name "*.service" -exec basename {} \; ) All"
PS3="Please select a service: "
select service in $services
do
  echo $REPLY and $service
  if [[ "$service" == "All" ]]
  then
    for s in $services
    do
      path=$(readlink -f $(find  $_COMPOSE_PATH -name "$s"))
      echo "$path"

      #echo "creating link : ln -s $path $_SERVICE_PATH/$service"
      ln -s $path $_SERVICE_PATH/$s
      systemctl --system daemon-reload
      systemctl start $s
    done
  else
    path=$(readlink -f $(find  $_COMPOSE_PATH -name "$service"))
    echo "$path"

    #echo "creating link : ln -s $path $_SERVICE_PATH/$service"
    ln -s $path $_SERVICE_PATH/$service
    systemctl --system daemon-reload
    systemctl start $service
  fi
done
