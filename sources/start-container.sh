#!/bin/bash

#Let's load some functions...

start-container(){
  if [[ $# -le "1" ]]; then
    logE "Usage : You must give at least the image name..."
    return
  fi

  if ! get_available_images | grep "$1"
  then
    logE "The given image connot be found ! "
    return
  fi

  if [[ "$3" == "--save" ]]; then
    _save_image=1
  fi

  if [[ -z $3 ]] && [[ "$2" == "--save" ]]; then
    _save_image=1
  fi

  _current_image=$(sed  -e's/[ \t]*$//' <(echo $1) )
  _custom_config=$2
  _image_path=$RELEASE_PATH/$1
  _image_tmp_path=$TMP_PATH/$USER/$_current_image-$(basename $(mktemp))

  mkdir -p $_image_tmp_path


  logD "Loadding configuration with $_custom_config config"

  if [[ -n "$_custom_config" ]]; then
    logI "Loading configuration..."
    load_image_configuration $_custom_config
    _print_configuration_state
  else
    logW "No run config given. "
  fi



  logD "Extracting each layers..."
  extract-image-layers $_current_image

  logD "Setup networking features.."
  image-set-network
  logD "Setup specific configuration.."
  image-set-config $_image_tmp_path

  logD "Finaly start the image !!"
  start_image $_image_tmp_path

  #We wait until the end of the container
  logD "Image ended ! time to reverse modifications ..."
  remove_redirections
  if [[ "$_save_image" -ne 1 ]]; then
    clear_fs $_image_tmp_path
  fi
  unload_image_configuration
}

#Extract <name> to <folder>
#

extract-container(){
  if [[ $# -lt "1" ]]; then
    logE "Usage : You must give at least the image name..."
    return
  fi

  if ! get_available_images | grep "$1"
  then
    logE "The given image connot be found ! "
    return
  fi

  local extracted=$(sed  -e's/[ \t]*$//' <(echo $1) )

  if [[ -z "$2" ]]; then
    _image_tmp_path=$TMP_PATH/$USER/$_current_image-$(basename $(mktemp))
  else
    _image_tmp_path=$2
  fi

  mkdir -p $_image_tmp_path

  logI "The Path is $_image_tmp_path"

  logD "Extracting each layers... of $1"
  extract-image-layers $extracted
}
