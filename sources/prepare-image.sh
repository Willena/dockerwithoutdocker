#!/bin/bash
prepare-image(){
  if [ "$#" -le "2" ]
  then
          echo "Usage : $0 [image:v@digest] [folder-dest] [-force]"
          exit 1
  fi

  echo "Step (1) : Preparing folders..."
  if [ -e "$2" ]
  then
          if [ "x$3" != "x-force" ]
          then
                  echo "Info: The directory $2 already exist, if you wan't to delete it anyway add "-force" to the commande line"
                  exit 1
          fi

          echo "Info: Removing folder $2"
          rm -rf "$2"
  fi

  mkdir -p "$2"



  echo "Step (2) : Download image from the registry..."
  TMP_DIR=$TMP_PATH/$(basename $(mktemp))

  download-frozen-image-v2 "$TMP_DIR" "$1"

  if [ $? -ne 0 ]
  then
          echo "Step (1) : Failed !"
          exit 1
  fi

  echo "Step (3) : Extracting data ..."
  LAYERS=$(jq '.[0].Layers' "$TMP_DIR/manifest.json" | sed 's/\,//gm' | sed 's/\[//gm' | sed 's/\]//gm' | xargs | tr ' ' '\n')

  for layer in $LAYERS
  do
          echo "Extracting layer $layer"
          pv "$TMP_DIR/$layer" | tar -xzf - -C "$2"
          if [ $? -ne 0 ]
          then
                  echo "Step (3) : Failed !"
                  exit 1
          fi
  done

  echo "Step (4): Cleaning..."
  rm -rf "$TMP_DIR"

  echo " Success ! Your container is available "
}
