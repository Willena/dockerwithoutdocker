#!/bin/bash

# This file will describe some function used for updating ufw and port redirections
_check_ports(){
	local re='^[0-9]+$'
	local ports=$@
	if [[ "$#" -eq "0" ]]
	then
		echo "No port provided..."
		return 1
	fi

	logD " Checking provided ports $@"

	for p in $ports
	do
		logD "-- new entry -- "
		logD "value = $p"
		if ! [[ $p =~ $re ]] ; then
		   echo "Invalid port '$p'"
		   return 1
		fi

		if [[ "$p" -gt "65535" ]] || [[ "$p" -lt "0" ]]
		then
			echo "Port not in range ( 0-65535 )"
			return 1
		fi

	done

	return 0
}

expose_port(){
  if [[ -z $_exposed_ports ]]; then
    logI "No port redirection"
  fi

	logD "Port list : $_exposed_ports"

  for p in $_exposed_ports
  do
		logD "-- Imput port : $p -- "
    set $(echo $p | tr ':' ' ')

    hostPort=$1
    containerPort=$2

		logD "  Ports : part1: $hostPort part2: $containerPort"

  	if [[ "$hostPort" =~ *"-"* ]] && [[ "$containerPort" =~ *"-"* ]]
  	then
			logD "  Range port detected ! "
			logD "  redirect_range $(echo $hostPort $containerPort | tr '-' ' ')"
      redirect_range $(echo $hostPort $containerPort | tr '-' ' ')
  		return
  	fi

  	if [[ "$hostPort" =~ *"/"* ]] && [[ "$containerPort" =~ *"/"* ]]
  	then
			logD "  Single port with protocol detected"
			logD "  redirect_proto $(echo $hostPort $containerPort | tr '/' ' ')"
      redirect_proto $(echo $hostPort $containerPort | tr '/' ' ')
  		return
  	fi

		logD "  Proxy or single ?"
  	if  echo "$hostPort" | grep "proxy"
  	then
			logD "  It's proxy :) we are proxing to $containerPort"
			logD "  proxy_to $containerPort"
			local proto=$(echo $hostPort |  tr '+' ' ' | cut -d' ' -f2)
  	  proxy_to $containerPort $proto
  	else
			logD "  In fact, single port is good :) From $hostPort to $containerPort"
			logD "  redirect_single $hostPort $containerPort"
  		redirect_single $hostPort $containerPort
  	fi
  done
}

proxy_to(){
  local containerPort=$1
	local proto=$2

  if ! _check_ports $containerPort ; then
    logE "You must provide a valid port..."
    return
  fi

  if [[ -z $_net_ip ]]; then
    logE "Network should be define before Exposing ports"
		return
  fi

  if [[ -z $_service_name ]]; then
    logE "You must define a service name before proxing ports"
		return
  fi

	if [[ "$proto" == "http" ]]; then
		curl "http://master-nginx:3000/add?ip=$_net_ip&port=$containerPort&proto=http&name=$_service_name"
		return
	elif [[ "$proto" == "https" ]]; then
		curl "http://master-nginx:3000/add?ip=$_net_ip&port=$containerPort&proto=https&name=$_service_name"
		return
	fi

  logW "Proxy is not currently working... But it will be fine soon :p"

}

# Input <p1> <p2> <p3> <p4>
# equivalent : p1-p2:p3-p4
redirect_range(){
  if [[ $_net_type == "private" ]] || [[ $_net_ip =~ "127."* ]]; then
    logE "Redirection refused ! Forbiden to redirect to 127.0.0.1"
    return
  fi

  if ! _check_ports $1 $2 $3 $4; then
    logE "Cannot work with less than 4 valid port..."
    return
  fi

  logI "iptables -t nat -A PREROUTING -i $DEFAULT_IFACE --dport $1:$2 -j DNAT --to-destination $_net_ip:$3-$4"
  iptables -t nat -A PREROUTING -i $DEFAULT_IFACE -p tcp --dport $1:$2 -j DNAT --to-destination $_net_ip:$3-$4
	iptables -t nat -A PREROUTING -i $DEFAULT_IFACE -p udp --dport $1:$2 -j DNAT --to-destination $_net_ip:$3-$4

	iptables -t nat -A POSTROUTING -o $DEFAULT_IFACE -p tcp -d $_net_ip --dport $3:$4 -j SNAT --to-source 195.181.244.242:$1:$2
	iptables -t nat -A POSTROUTING -o $DEFAULT_IFACE -p udp -d $_net_ip --dport $3:$4 -j SNAT --to-source 195.181.244.242:$1:$2

	if [[ $? -ne 0 ]]; then
		logE "Cannot set iptable rules ... "
		return
	fi
}



#Input: <p1> <p2>
redirect_single(){
  if [[ $_net_type == "private" ]] || [[ $_net_ip =~ "127."* ]]; then
    logE "Redirection refused ! Forbiden to redirect to 127.0.0.1"
    return
  fi

  if ! _check_ports $1 $2 ; then
    logE "Invalid ports given !"
    return
  fi

  iptables -t nat -A PREROUTING -i $DEFAULT_IFACE -p tcp --dport $1 -j DNAT --to-destination $_net_ip:$2
	iptables -t nat -A PREROUTING -i $DEFAULT_IFACE -p udp --dport $1 -j DNAT --to-destination $_net_ip:$2

	iptables -t nat -A POSTROUTING -o $DEFAULT_IFACE -p tcp -d $_net_ip --dport $2 -j SNAT --to-source 195.181.244.242:$1
	iptables -t nat -A POSTROUTING -o $DEFAULT_IFACE -p udp -d $_net_ip --dport $2 -j SNAT --to-source 195.181.244.242:$1

	if [[ $? -ne 0 ]]; then
		logE "Cannot set iptable rules ... "
		return
	fi

}

# Input: <proto> <p1> <proto> <p2>
redirect_proto(){
  if [[ $_net_type == "private" ]] || [[ $_net_ip =~ "127."* ]]; then
    logE "Redirection refused ! Forbiden to redirect to 127.0.0.1"
    return
  fi

  if [[ -z "$1" ]] || [[ -z "$2" ]] || [[ -z "$3" ]] || [[ -z "$4" ]]; then
    logE "Cannot work with invalid data..."
    return
  fi

  if _check_ports $2 $4 ; then
    logE "Invalid ports !"
    return
  fi

  if [[ "$1" != "$2" ]]; then
    logE "It is not possible to convert tcp to udp like that..."
		return
  fi

  iptables -t nat -A PREROUTING -i $DEFAULT_IFACE -p $1 --dport $2 -j DNAT --to-destination $_net_ip:$4
	iptables -t nat -A POSTROUTING -o $DEFAULT_IFACE -p $1 -d $_net_ip --dport $4 -j SNAT --to-source 195.181.244.242:$2

	if [[ $? -ne 0 ]]; then
		logE "Cannot set iptable rules ... "
		return
	fi
}


remove_redirections(){
	## Removing iptables Rules ...
	eval $(iptables -t nat  -S | grep "$_net_ip" | sed 's/-A /-D /' | sed 's/^/iptables -t nat /' | sed 's/$/ \&\& /' | tr '\n' ' ' | sed -r  's/ \&\&\s*$//')
	sed -i "s/^${_net_ip//\./\\.}.*//" /etc/hosts
	curl "http://master-nginx:3000/del?ip=$_net_ip&name=$_service_name"
	## Removing proxy if visible...

}
