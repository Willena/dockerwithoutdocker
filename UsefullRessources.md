Systemd-nspawn
==============

-	https://www.freedesktop.org/software/systemd/man/systemd-nspawn.html
-	http://chimeracoder.github.io/docker-without-docker/
-	https://wiki.archlinux.org/index.php/Systemd-nspawn
-	https://wiki.archlinux.org/index.php/Init#systemd-nspawn

-	tty control fix : https://github.com/systemd/systemd/issues/1431#issuecomment-197586093

Network
=======

https://askubuntu.com/questions/444124/how-to-add-a-loopback-interface

IMAGES
======

-	rtorrent flood rutorrent

	-	Multi user pull request : https://github.com/jfurrow/flood/pull/549

-	docker-openvpn : https://github.com/kylemanna/docker-openvpn
