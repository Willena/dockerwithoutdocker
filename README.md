Server Images 
=============

Principle 
-------

The idea behind is to enable the possibility to restart a full server from scratch with all services configured. ( data may still have been lost :(  but quick and easy restart ! )

This repo has been created to contain some docker images but customized to fullfill the limitation of the server.
These limitation are:
 - no possibility to run docker
 - no possibility to isolate network interfaces
 - no possibility to create network interfaces
 
Instead the systemd-nspawn command is used to start small linux container, that are docker images.

These image are customised since ports cannot be proxyfied/redirected. With our server limitation we are limited to one instance per port and no loadballancing features.
The current server limitation imply that we have to change edit container configs to be usable with a single interface for all cointainer.

Container must also listen on the localhost interface 127.0.0.1, so that privates services such as database, rtorrent servers, .. are kept secret.
If some access to the exterior is needed, we'll proxy the data throught the firewall or throught apache modproxy.

Usage 
------

To be used, the repo needs to be cloned.
Then just start the `install.sh` script. It will prepare some dependencies of the system, install start at boot sequences, a simple manager for these services.

A special image called "systemd-nspawn-manager" will try to download another repo that container image startup configurations that will feed to interface with some basic elements. 
These elements can be mount points, environnement variables, ...

Structure
----------

One folder per image. /<imagename>/*

