#!/bin/bash

build-image(){
  if [[ $# -ne "1" ]]; then
    logE "Usage : You must give at least the image conf file... and a name"
    return
  fi

  if [[ -n "$2" ]]; then
    if echo "$(get_available_images)" | grep "$2"
    then
      logE "The given image already exist. Please force a name "
      return
    fi
  fi

  _custom_config=$1
  _current_image="$(sed  -e's/[ \t]*$//' <(echo $2) )"



  logD "Loadding configuration with $_custom_config config"
  load_image_configuration $_custom_config
  if [[ -z "$_current_image" ]]; then
    if [[ -z "$_name_line" ]]; then
      logE "You must provide an image name in the building conf;"
      return
    fi

    _current_image="$(sed  -e's/[ \t]*$//' <(echo $_name_line) )"

  fi

  _image_path="$BUILD_PATH/$_current_image"

  _print_configuration_state

  #Add the possibility to use on disk image to build other images.
  # Extract all layer of the "from" image.
  # Then execute anyting left.
  logD "The _source_image is $_source_image"
  if get_available_images | grep "$_source_image"
  then
    logI "Extracting known image '$_source_image'"
    extract-container $_source_image $_image_path
  else
    logD "Downloading base image from docker HUB"
    logW "prepare-image $_source_image $_image_path"
    if ! prepare-image $_source_image $_image_path -force
    then
      logE "Base image cannot be downloaded ! Exiting ..."
      return
    fi
  fi

  logD "Creating a base layer."
  image-create-layer $_image_path

  logD "Download and extract ok !"
  image-set-layers
  image-copy
  image-run-commands
  image-set-entrypoint

  logD "Image successfully built !"

  clear_fs $_image_path
  unload_image_configuration
}
