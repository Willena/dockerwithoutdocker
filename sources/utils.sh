#!/bin/bash

export ROOT_PATH="/root/images"

source bash-colors.sh



# Input <messages> <messages>
logE(){
  clr_red "$@"
  _has_Error=1
}

logI(){
  clr_cyan "$@"
}

logW(){
  clr_brown "$@"
  _has_warning=1
}

logD(){
  if [[ -z "$DEBUG" ]]; then
    return
  fi
  echo -e "\e[35m DEBUG: $@ \e[0m"
}

#Initialize some folders !
mkdir -p $ROOT_PATH/build
mkdir -p $ROOT_PATH/tmp
mkdir -p $ROOT_PATH/release

# Export some variables
export TMP_PATH=$ROOT_PATH/tmp
export RELEASE_PATH=$ROOT_PATH/release
export BUILD_PATH=$ROOT_PATH/build

export DEBUG=1
