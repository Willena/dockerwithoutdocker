#!/bin/bash

_env_line=()
_volume_line=""
_volumero_line=""
_name_line=""
_entry_point_line=""
_net_line=""
_service_name=""
_net_ip="127.0.0.1"
_net_type="private"
_proxy_backend="apache"
_source_image=$1

_exposed_ports=""
_config_set=()
_copy=()
_run_cmds=()
_added_layers=""


NETWORK(){
  if [[ $1 == "shared" ]]; then
    logI "Your on a shared net"
    _net_type="shared"
    return
  fi

  #Default is a full private network
  logI "You'r on a private net"
  _net_type="private"
  _net_line=" --private-network "
}

ENV(){

  echo "\$1 is '$1'"
  local var="$1"

  echo "\$var is '$var'"

  if [[ -z $var ]]; then
    logI "Usage: ENV <var>=<value>"
    return
  fi

  if ! echo $var | grep '=' ; then
    logI "Usage: ENV <var>=<value>"
    return
  fi

  local v2=$(echo $var | tr '=' ' ')
  set $v2
  echo "\$v2 is '$v2'"

  local name=$1
  shift
  local value=$@

  echo "\$name is '$name'"
  echo "\$value is '$value'"

  if [[ -z $name ]] || [[ -z $value ]]; then
    logI "Usage: ENV <var>=<value>"
    return
  fi

  _env_line+=("$name=$value")

}

COPY(){

  if [[ -z "$1" ]] || [[ -z "$2" ]]; then
    logE "COPY must have 2 operand."
    return
  fi

  _copy+=("$1 $2")

}

RUN(){

  if [[ -z "$@" ]]; then
    logE "Run must have a command ..."
    return
  fi

  _run_cmds+=("$@")

}

VOLUME(){
  if [[ -z "$1" ]]; then
    logI "Usage : VOLUMES <hostPath>:<containerPath>"
    return
  fi

  _volume_line=" $_volume_line $1 "
}

VOLUME-RO(){
  if [[ -z "$1" ]]; then
    logI "Usage : VOLUMES <hostPath>:<containerPath>"
    return
  fi

  _volumero_line=" $_volumero_line $1 "
}

NAME(){
    if [[ -z "$1" ]]; then
      logI "Usage: NAME <name>"
      return
    fi

    _name_line="$1"
}

SERVICE_NAME(){
  if [[ -z "$1" ]]; then
    logI "Usage: SERVICE_NAME <name>"
    return
  fi
  _service_name="$1"
}

ENTRYPOINT(){
  if [[ -z "$@" ]]; then
    logI "Usage: ENTRYPOINT <command>"
    return
  fi
  _entry_point_line=" $@ "
}

VERSION(){
  if [[ -z "$1" ]]; then
    logI "Usage: VERSION <version>"
    return
  fi
  _version=$1
}

_expose_usage(){
	logI "Usage : EXPOSE proxy:<ports>"
	logI "Usage : EXPOSE tcp/<portA>:tcp/<portB>"
	logI "Usage : EXPOSE udp/<partA>:udp/<portB>"
	logI "Usage : EXPOSE <portA>:<portB>"
	logI "Usage : EXPOSE <PortA-PortB>:<PortC-PortD>"
}

EXPOSE(){

	local req=$1
	if [[ -z "$req" ]]; then
    _expose_usage
    return
	fi

	set $(echo $req | tr ':' ' ')
  hostPort=$1
  containerPort=$2
  #We must have 2 part in the port redirection
	if [[ "$#" -ne "2" ]]; then
    echo "second"
		_expose_usage
    return
	fi

  _exposed_ports=" $_exposed_ports $req"


}

FROM(){
  if [[ -z "$1" ]]; then
    logI "Usage: FROM <repo>/<image>:<version>"
    return
  fi
  logI "$1"
  _source_image=$1
}

PRE_CONFIG(){

  command=$1
  if [[ -z "$command" ]]; then
    logI "Usage: PRE_CONFIG <command>"
    logI "Usage: PRE_CONFIG setip <file>"
    return
  fi

  logI "$1"

  _config_set+=("$1")

}

ADD_LAYER(){

  layername=$1
  if [[ -z "$layername" ]]; then
    logI "Usage: ADD_LAYER <layername>"
    return
  fi

  _added_layers=" $layername "

}


_print_configuration_state(){
  echo "IMAGE SOURCE:"  "$_source_image"
  echo "ENV:"  "${_env_line[@]}"
  echo "VOLUMES:"  "$_volume_line"
  echo "ENTRYPOINT:"  "$_entry_point_line"
  echo "NETWORK:"  "$_net_ip $_net_type $_net_line"
  echo "PROXY BACKEND:"  "$_proxy_backend"
  echo "SERVICE NAME:"  "$_service_name"
  echo "EXPOSE-PORT:"  "$_exposed_ports"
  echo "PRE-CONFIG:"  "${#_config_set[@]} ${_config_set[@]}"
  echo "LAYERS:"  "$_added_layers"
}
